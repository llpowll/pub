#!/usr/local/bin/expect
set timeout 1 
set 10.0.0.100 [lindex $argv 0]

set username "XXX" 
set password "XXX" 
set epassword "XXX" 

# access point
spawn telnet 10.0.0.100
	expect "Username:"
	send "$username\n"
	expect "Password:"
	send "$password\n"
		expect ">"
		send "en\n"
		expect "Password:"
		send "$epassword\n"
			expect "#"
			send "conf ter\n"
			expect "(config)#"
			send "in fa0/15\n"
			expect "(config-if)#"
			send "po in ne\n"
			expect "(config-if)#"
			send "no po in ne\n"
			expect "(config-if)#"
			send "exit\n"
			expect "(config)#"
			send "exit\n"
			expect "#"
			send "exit\n"
  interact
}
